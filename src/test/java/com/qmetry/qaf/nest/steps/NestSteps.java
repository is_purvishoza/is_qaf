package com.qmetry.qaf.nest.steps;
import static com.qmetry.qaf.automation.step.CommonStep.*;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.WebDriverTestBase;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class NestSteps extends WebDriverBaseTestPage<WebDriverTestPage> {
	
	@Override
	protected void openPage(PageLocator locator, Object... args) {
	
	}
	@FindBy(locator="input.loader.button")
	private QAFWebElement inputLoaderButton;
	
	@QAFTestStep(description = "get infostretch url")
	public void getURL() {
		 new WebDriverTestBase().getDriver().get("http://10.12.40.86/nest_security_testing/");
		 new WebDriverTestBase().getDriver().manage().window().maximize();
	}
	@QAFTestStep(description="se")
	public void sendUsername() {
		
		waitForPresent("input.username.search");
		sendKeys("ashish.biradar1","input.username.search");
	}
	
	@QAFTestStep(description = "send blank username data")
	public void sendBlankUsername() {
		
		//inputPasswordSearch.waitForPresent();
		sendKeys("","input.username.search");
	}
	
	@QAFTestStep(description="send password data")
	public void sendPassword() {
		
		sendKeys("Test@1234", "input.password.search");

	}
	@QAFTestStep(description = "send blank password data")
	public void sendBlankPassword() {
		
		//inputPasswordSearch.waitForPresent();
		sendKeys("","input.password.search");
		
	}
	@QAFTestStep(description="login with {username} and {password}")
	public void submitData(String username, String password) {
		
		sendKeys(username,"input.username.search");
		sendKeys(password, "input.password.search");
		waitForVisible("input.login.button");
		click("input.login.button");
		inputLoaderButton.verifyNotVisible();
	}
	
	@QAFTestStep(description="logout from site")
	public void submitData() {

		inputLoaderButton.verifyNotVisible();
		waitForVisible("input.logout.button");
		click("input.logout.button");
	}
	
}
