package com.qmetry.qaf.nest.test;

import org.testng.annotations.Test;

import com.qmetry.qaf.nest.page.NestHomePage;
import com.qmetry.qaf.automation.ui.WebDriverTestCase;

public class NestHome extends WebDriverTestCase{
	@Test(description="Nest Home Test", groups={"SMOKE"})
	public void nestHome() throws InterruptedException {
		NestHomePage nestHomePage = new NestHomePage();
		nestHomePage.getUrl();
		nestHomePage.loginWithCredentials("gaurav.parekh", "gaurav.parekh");
		//nestHomePage.loginWithCredentials("", "");
		nestHomePage.getLoggedinuser().assertVisible();
		nestHomePage.chageView();
		nestHomePage.selectOptionFromSidebar();
		nestHomePage.getBreadcrumText().verifyVisible();
		//nestHomePage.logoutFromHome();
		//nestHomePage.verifyLogout();
		//nestHomePage.verifyTextOfEmptyCredentails();
	}
}
