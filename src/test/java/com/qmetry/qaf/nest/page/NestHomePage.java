package com.qmetry.qaf.nest.page;

import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.WebDriverTestBase;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver; 

public class NestHomePage extends WebDriverBaseTestPage<WebDriverTestPage>{

	@FindBy(locator = "input.username.search")
	private QAFWebElement usernamebox;
	
	@FindBy(locator = "input.password.search")
	private QAFWebElement passwordbox;
	
	@FindBy(locator = "input.login.button")
	private QAFWebElement loginbutton;
	
	@FindBy(locator = "div.nest.loggedusername")
	private QAFWebElement loggedinuser;
	
	@FindBy(locator="input.loader.button")
	private QAFWebElement loader;
	
	@FindBy(locator = "input.logout.button")
	private QAFWebElement logoutuser;
	
	@FindBy(locator = "input.text")
	private QAFWebElement EmptyData;
	
	@FindBy(locator = "input.changeview.button")
	private QAFWebElement ChangeViewButton;
	
	@FindBy(locator = "input.sidebar.button")
	private QAFWebElement Sidebar;
	
	@FindBy(locator = "input.navigation.button")
	private QAFWebElement NavigationButton;
	

	@FindBy(locator = "input.publisher.button")
	private QAFWebElement PublisherButton;
	
	@FindBy(locator = "input.myposts.button")
	private QAFWebElement MyPostButton;
	
	@FindBy(locator = "input.breadcrum.text")
	private QAFWebElement MyBreadcrumButton;
	
	public QAFWebElement getUsernamebox() {
		return usernamebox;
	}

	public QAFWebElement getPasswordbox() {
		return passwordbox;
	}

	public QAFWebElement getLoginbutton() {
		return loginbutton;
	}
	
	public QAFWebElement getLoggedinuser() {
		return loggedinuser;
	}
	public QAFWebElement getBreadcrumText() {
		return MyBreadcrumButton;
	}
	public QAFWebElement navigator() {
		return NavigationButton;
	}
	
	public void checkLoggedInUser() {
		loggedinuser.waitForVisible();
	}
	
	public void getUrl() {
		 new WebDriverTestBase().getDriver().get("http://10.12.40.86/nest_security_testing/");
		 new WebDriverTestBase().getDriver().manage().window().fullscreen();
	}
	
	@QAFTestStep(description="login with {username} and {password}")
	public void loginWithCredentials(String userName, String pwd) {
		usernamebox.sendKeys(userName);
		passwordbox.sendKeys(pwd);
		loginbutton.waitForEnabled();
		loginbutton.click();
		//loader.waitForNotVisible();
		
	}

	
	@QAFTestStep(description="Change the view from My View to ManagerView")
	public void chageView() {
		loader.waitForNotVisible();
		ChangeViewButton.click();
		loader.waitForNotVisible(40000);			
	}
	
	@QAFTestStep(description="Select Publisher option form Sidebar")
	public void selectOptionFromSidebar() throws InterruptedException
	{
		NavigationButton.click();
		Thread.sleep(5000);
		WebElement element = driver.findElement(By.xpath("//li[contains(@ng-show, 'Manager View')]//span[contains(text(),'Publisher')]"));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
		//Thread.sleep(5000); 
		element.click();
		WebElement element1 = driver.findElement(By.xpath("//div[@id = 'sidebar-wrapper']//li[contains(@ng-show, 'Manager View')]//span[contains(text(),'Publisher')]/../ul/li/a[contains(text(),'My Posts')]"));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element1);
		Thread.sleep(5000);
		element1.click();
		Thread.sleep(5000);
		
	}

	
	@QAFTestStep(description="verification of empty credentials user")
	public void verifyTextOfEmptyCredentails() {
		
		if(EmptyData.isDisplayed())
		{
			System.out.println(EmptyData.getText());
		}
		}

	
	@QAFTestStep(description="verification of logged out user")
	public void verifyLogout() {
		loader.waitForNotVisible();
		loginbutton.waitForVisible();
	}

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub
		
	}

}
