package com.qmetry.qaf.example.steps;

import static com.qmetry.qaf.automation.step.CommonStep.click;
import static com.qmetry.qaf.automation.step.CommonStep.sendKeys;
import static com.qmetry.qaf.automation.step.CommonStep.waitForPresent;
import static com.qmetry.qaf.automation.step.CommonStep.verifyVisible;


import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverTestBase;



public class StepsLibrary {

	@QAFTestStep(description = "get url")
	public static void getURL() {
		 new WebDriverTestBase().getDriver().get("https://www.google.com/");
	}
	
	
	/**
	 * @param searchTerm
	 *            : search term to be searched
	 */
	
	@QAFTestStep(description = "search for {0}")
	public static void searchFor(String searchTerm) {
		sendKeys(searchTerm, "input.search");
		waitForPresent("button.search");
		click("button.search");
	}
	
	
}
